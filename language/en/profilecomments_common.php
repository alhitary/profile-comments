<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'PROFILE_COMMENT'  => 'Profile Comment',
	'PROFILE_COMMENTS' => 'Profile Comments',

	'PROCOM_POST_COMMENT'     => 'Post Comment'
));
