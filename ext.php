<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\profilecomments;

class ext extends \phpbb\extension\base
{
	function enable_step($old_state)
	{
		switch($old_state)
		{
			default:
				return parent::enable_step($old_state);
				break;
		}
	}

	function disable_step($old_state)
	{
		switch($old_state)
		{
			default:
				return parent::disable_step($old_state);
				break;
		}
	}

	function purge_step($old_state)
	{
		switch($old_state)
		{
			default:
				return parent::purge_step($old_state);
				break;
		}
	}
}