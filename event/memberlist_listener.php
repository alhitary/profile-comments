<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\profilecomments\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class memberlist_listener implements EventSubscriberInterface
{
	/** @var \phpbb\auth\auth */
	protected $auth;
	/** @var \phpbb\db\driver\driver_interface */
	protected $db;
	/** @var \phpbb\request\request */
	protected $request;
	/** @var \phpbb\template\template */
	protected $template;
	/** @var \phpbb\user */
	protected $user;

	/** @var string */
	protected $pc_table;

	/**
	* Constructor
	*/
	public function __construct(\phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\request\request $request, \phpbb\template\template $template, \phpbb\user $user, $profile_comments_table)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->request = $request;
		$this->template = $template;
		$this->user = $user;

		$this->pc_table = $profile_comments_table;
	}

	/**
	* Listens for core event calls.
	*
	* @return array   Array containing subscribed core events associated with callbacks. 
	*/
	static public function getSubscribedEvents()
	{
		return array(
			'core.memberlist_view_profile'          => 'prepare_display',
		);
	}

	/**
	* Handles essential profile comment logic.
	*
	* @var $event  Contextual data.
	*/
	public function prepare_display($event)
	{
		$errors = array();
		$member = $event['member'];

		if($this->request->is_set_post('submit'))
		{
			// User posted a comment.
			if($this->auth->acl_get('u_procom_post'))
			{
				$new_post_data = array(
					'comment_user_id_from'  => $this->user->data['user_id'],
					'comment_user_id_to'    => $member['user_id'],
					'comment_message'       => $this->request->variable('message', ''),
					'comment_time'          => time(),
				);
				$sql = 'INSERT INTO ' . $this->pc_table .'
				 	' . $this->db->sql_build_array('INSERT', $new_post_data);
				$this->db->sql_query($sql);
			}
			else
			{
				$errors[] = $this->user->lang('PROCOM_NO_POST_PERMISSION');
			}
		}

		// Grab users' comments.
		$sql = 'SELECT pc.*, u.username, u.user_colour
			FROM ' . $this->pc_table . ' pc
			LEFT JOIN ' . USERS_TABLE . ' u
				ON u.user_id = pc.comment_user_id_from
			WHERE pc.comment_user_id_to = ' . $member['user_id'] . ' ';

		// If user shouldn't see them, exclude soft-deleted posts.
		if(!$this->auth->acl_get('m_procom_view_soft_del'))
		{
			$sql .= 'AND comment_soft_deleted = 0 ';
		}

		$sql .= 'ORDER BY pc.comment_time DESC';

		$result = $this->db->sql_query($sql);

		// Get all the comments from the result and hand to template block var.
		while($row = $this->db->sql_fetchrow($result))
		{
			$from_username = get_username_string('full', $row['comment_user_id_from'], $row['username'], $row['user_colour']);

			$this->template->assign_block_vars('profile_comments', array(
				'ID'            => $row['comment_id'],
				'USER_ID_FROM'  => $row['comment_user_id_from'],
				'USERNAME_FROM' => $from_username,
				'MESSAGE'       => $row['comment_message'],
				'SOFT_DELETED'  => ($row['comment_soft_deleted'] == 1),
				'TIME'          => $this->user->format_date($row['comment_time']),
			));
		}

		if(count($errors) > 0)
		{
			foreach($errors as $key => $value)
			{
				$this->template->assign_block_vars('procom_errors', array(
					'MESSAGE' => $value
				));
			}
		}

		$this->template->assign_vars(array(
			'S_PROCOM_ERROR'          => (count($errors) > 0),
			'S_PROCOM_POST'           => $this->auth->acl_get('u_procom_post'),
			'S_PROCOM_VIEW'           => $this->auth->acl_get('u_procom_view'),
			'S_PROCOM_SOFT_DEL_VIEW'  => $this->auth->acl_get('m_procom_view_soft_del'),
			'S_PROCOM_SOFT_DEL'       => $this->auth->acl_get('m_procom_soft_del'),
			'S_PROCOM_HARD_DEL'       => $this->auth->acl_get('m_procom_hard_del'),
		));
	}
}