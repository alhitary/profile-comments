<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\profilecomments\event;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class main_listener implements EventSubscriberInterface
{
	/**
	* Constructor
	*/
	public function __construct()
	{
	}

	/**
	* Listens for core event calls.
	*
	* @return array   Array containing subscribed core events associated with callbacks. 
	*/
	static public function getSubscribedEvents()
	{
		return array(
			'core.user_setup'  => 'language_setup',
		);
	}

	/**
	* Adds common language files.
	*
	* @param array  $event   Contains contextual information from event.
	*/
	public function language_setup($event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = array(
			'ext_name' => 'forumpromotion/profilecomments',
			'lang_set' => 'profilecomments_common'
		);
		$event['lang_set_ext'] = $lang_set_ext;
	}
}