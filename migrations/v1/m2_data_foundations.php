<?php
/**
*
* Profile Comments
*
* @copyright (c) 2015 Forum Promotion
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

namespace forumpromotion\profilecomments\migrations\v1;

class m2_data_foundations extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return array(
			array('permission.add', array('m_procom_soft_del')),
			array('permission.add', array('m_procom_hard_del')),
			array('permission.add', array('m_procom_view_soft_del')),

			array('permission.add', array('u_procom_post')),
			array('permission.add', array('u_procom_view')),

			array('permission.permission_set', array('ROLE_ADMIN_STANDARD', 'a_crml_manage_config')),
			array('permission.permission_set', array('ROLE_MOD_FULL', array('m_procom_soft_del', 'm_procom_view_soft_del', 'm_procom_hard_del'))), 

			array('permission.permission_set', array('ROLE_USER_STANDARD', array('u_procom_post', 'u_procom_view'))),
			array('permission.permission_set', array('ROLE_USER_FULL', array('u_procom_post', 'u_procom_view'))),
		);
	}
}